#+TITLE: Geschichte
#+AUTHOR: Jeldo Arno Meppen

* Deutschland nach 1945

** DONE Vergangenheitsbewältigung
   - populäres, aber umstrittenes Wort
   - ungenau, da Vergangenheit nicht mehr bewältigt werden kann, nur ihre Folgen
   - offenbart nach SCHLINK eine "Sehnsucht nach dem Unmöglichen"
     - Sühne der Schuld
     - Wiedergutmachung
   - ein scheinbar exklusiv deutsches Problem

** TODO Vergangenheitspolitik
   - politischer Prozess
   - hohe gesellschaftliche Akzeptanz
   - "geradezu kollektiv erwartet"
   - Strafaufhebungen und Integrationsmaßnahmen für NSDAP-Mitglieder
     - Rückführung in 
