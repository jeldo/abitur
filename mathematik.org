
#+TITLE: Mathematik
#+AUTHOR: Jeldo Arno Meppen

* Algebra
** Exponenten
*** Basis a und Exponent b

$$ a^b $$

*** 0 und 1 als Exponenten

$$ a^2 = 1 \cdot a \cdot a $$

$$ a^1 = 1 \cdot a $$

$$ a^0 = 1 $$

*** 0 als Basis

$$ 0^2 = 1 \cdot 0 \cdot 0 $$

$$ 0^1 = 1 \cdot 0 $$

$$ 0^0 \textrm{ is undefined} $$

*** -1 als Basis

$$ (-1)^0 = 1 $$

$$ (-1)^1 = -1 $$

$$ (-1)^2 = 1 $$

$$ (-1)^3 = -1 $$

*** Dezimale als Basis

$$ 0.2^3 = 0.2 \cdot 0.2 \cdot 0.2 = 0.008 $$

$$ 0.9^2 = 0.9 \cdot 0.9 = 0.81 $$

*** 

* Funktionen
* Vektoren
* Wahrscheinlichkeit
